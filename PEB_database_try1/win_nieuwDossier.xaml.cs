﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_nieuwDossier.xaml
    /// </summary>
    public partial class win_nieuwDossier : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database
        string currentDossierNummer = "";
        int DossierID;

        public win_nieuwDossier()
        {
            InitializeComponent();

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            visibilityGrids();
            generateDossierNummer();
            initializeComboBoxes();
            fillListKlanten();
        }

        public void generateDossierNummer()
        {
            string peb = "PEB"; //Componenten dossiernaam string
            string year = DateTime.Today.Year.ToString(); //jaar in 4-digit format bv 2018
            year = year.Remove(0, 2); //delete eerste twee cijfers van jaar -> 2018 wordt 18
            string month = DateTime.Today.Month.ToString("00"); //Maand in 2-digit format

            string query = "SELECT DISTINCT DossierNummer FROM Dossier WHERE (DossierNummer LIKE '"+peb+year+month+"%')"; //Query die alle unieke dossiernummers selecteert die beginnen met een bepaald format

            DataTable usedNumbers = new DataTable(); //Definitie van datatable om straks de gebruikte dossiernummers in op te slaan

            using (connection = new SqlConnection(connectionString))
            using (SqlDataAdapter adapter = new SqlDataAdapter(query,connection))
            {
                adapter.Fill(usedNumbers); //Vul datatable
                usedNumbers.PrimaryKey = new DataColumn[] { usedNumbers.Columns["DossierNummer"] }; //Toevoegen primary key (niet echt betekenis maar een primary key is nodig voor het row.find commando)
            }

            string DossierNummer = ""; //Definitie van dossiernummer, dit wordt later ingevuld

            for (int i = 10; i < 100; i++) //Loop die de mogelijke dossiernummers afgaat totdat een ongebruikte is gevonden
            {
                DossierNummer = peb + year + month + i.ToString(); //Samenstellen naam door optellen van componenten

                DataRow dr = usedNumbers.Rows.Find(DossierNummer); //Zoek commando die checkt of het dossiernummer al is gebruikt

                if (dr == null) //Als het nummer nog niet is gebruikt (Nummer is dus niet gevonden in de database)
                {
                    break; //Verlaat for-loop wanneer er een nog niet bestaand nummer is gevonden
                }

                if (i == 99)
                {
                    MessageBox.Show("Het maximale aantal dossiers deze maand is bereikt, hierdoor is geen nieuw nummer aangemaakt"); // Als zelfs 99 al is gebruikt geef dan een melding dat het niet goed gaat
                    DossierNummer = peb + year + month;
                }
            }
            txtBox_dossierNummer.Text = DossierNummer;
        }

        public void initializeComboBoxes()
        {
            comboBox_TypeDossier.Items.Add("Rechtbank");
            comboBox_TypeDossier.Items.Add("Geschillencommissie");
            comboBox_TypeDossier.Items.Add("PEB");

            comboBox_typeKlant.Items.Add("Aanvrager");
            comboBox_typeKlant.Items.Add("Client aanvrager");
            comboBox_typeKlant.Items.Add("Wederpartij");
            comboBox_typeKlant.Items.Add("Client wederpartij");

            comboBox_partijNummer.Items.Add("Partij 1");
            comboBox_partijNummer.Items.Add("Partij 2");
        }

        private void comboBox_TypeDossier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            visibilityGrids();
        }

        public void visibilityGrids()
        {
            string selection = "";
            if (comboBox_TypeDossier.SelectedValue != null)
            {
                selection = comboBox_TypeDossier.SelectedValue.ToString(); //Keuze welke velden zichtbaar moeten worden
            }
            
            if (selection == "Rechtbank")
            {
                Grid_rechtbank.Visibility = Visibility.Visible;
                Grid_SGC.Visibility = Visibility.Collapsed;
                Grid_nietNodigBijSGC.Visibility = Visibility.Visible;                
            }
            else if (selection == "Geschillencommissie")
            {
                Grid_rechtbank.Visibility = Visibility.Collapsed;
                Grid_SGC.Visibility = Visibility.Visible;
                Grid_nietNodigBijSGC.Visibility = Visibility.Collapsed;
            }
            else
            {
                Grid_rechtbank.Visibility = Visibility.Collapsed;
                Grid_SGC.Visibility = Visibility.Collapsed;
                Grid_nietNodigBijSGC.Visibility = Visibility.Visible;
            }

            if (currentDossierNummer == "")
            {
                Grid_selectKlant.Visibility = Visibility.Collapsed;
                Grid_rechtbank.Visibility = Visibility.Collapsed;
                Grid_SGC.Visibility = Visibility.Collapsed;

                btn_addDossier.Visibility = Visibility.Visible;
                btn_saveDossier.Visibility = Visibility.Collapsed;
            }
            else
            {
                Grid_selectKlant.Visibility = Visibility.Visible;

                btn_addDossier.Visibility = Visibility.Collapsed;
                btn_saveDossier.Visibility = Visibility.Visible;
                if (comboBox_TypeDossier.Text == "Geschillencommissie")
                {
                    Grid_nietNodigBijSGC.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void txtBox_zoekNaam_Changed(object sender, TextChangedEventArgs e)
        {
            fillListKlanten();
        }

        public void fillListKlanten()
        {
            string query = "SELECT * FROM Klanten WHERE (Naam LIKE '" + txtBox_zoekNaam.Text + "%')"; //Query die je operatie definieert

            using (connection = new SqlConnection(connectionString))
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable klantTable = new DataTable();
                adapter.Fill(klantTable); //Display klanten welke overeenkomen met de selectie

                listView_klanten.SelectedValuePath = "Id";
                listView_klanten.ItemsSource = klantTable.DefaultView;
            }
        }

        private void btn_addKlant_Click(object sender, RoutedEventArgs e)
        {
            NieuweKlant winAddKlant = new NieuweKlant();    //Nieuwe window instantieren
            winAddKlant.Show(); //Openen van aangemaakte venster
        }

        private void btn_newKlant_Click(object sender, RoutedEventArgs e)
        {
            if (listView_klanten.SelectedValue == null)
            {
                MessageBox.Show("Er is nog geen klant geselecteerd!", "Ontbrekende selectie", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (comboBox_typeKlant.Text == "")
            {
                MessageBox.Show("Het type klant is nog niet ingegeven!", "Ontbrekend veld", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            string query = "SELECT * FROM Dossier WHERE Dossiernummer ='" + currentDossierNummer + "'"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable idTable = new DataTable();
                adapter.Fill(idTable);

                string ID = idTable.Rows[0]["Id"].ToString();
                DossierID = int.Parse(ID);
            }

            query = "INSERT INTO DossierKlant (DossierId, KlantId, Type, PartijNummer) VALUES (@DossierId, @KlantId, @Type, @PartijNummer)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@DossierId", DossierID); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@KlantId", listView_klanten.SelectedValue);
                command.Parameters.AddWithValue("@Type", comboBox_typeKlant.Text);
                command.Parameters.AddWithValue("@PartijNummer", comboBox_partijNummer.Text);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            showBoundKlanten();
            visibilityGrids();
        }

        public void showBoundKlanten()
        {
            string query = "SELECT Klanten.Naam, Klanten.Straat, Klanten.Huisnummer, Klanten.Plaats, DossierKlant.Type, DossierKlant.PartijNummer, DossierKlant.Id FROM (DossierKlant INNER JOIN Klanten ON DossierKlant.KlantId = Klanten.Id) WHERE DossierId = '" + DossierID + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable klantTable = new DataTable();
                adapter.Fill(klantTable);

                listView_selectedKlanten.SelectedValuePath = "Id";
                listView_selectedKlanten.ItemsSource = klantTable.DefaultView;
            }
        }

        private void btn_addDossier_Click(object sender, RoutedEventArgs e)
        {
            if (comboBox_TypeDossier.Text == "")
            {
                MessageBox.Show("Het type dossier is nog niet ingegeven!", "Ontbrekend veld", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            string query = "INSERT INTO Dossier (DossierNummer, DossierSoort) VALUES (@DossierNummer, @DossierSoort)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@DossierNummer", txtBox_dossierNummer.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@DossierSoort", comboBox_TypeDossier.Text);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            currentDossierNummer = txtBox_dossierNummer.Text; //Save dossiernummer
            visibilityGrids(); //Klant selectie-module tonen

            if (comboBox_TypeDossier.Text == "Geschillencommissie")
            {
                addSGC();
            }

            //Maak mappenstructuur aan:
            string path = PEB_database_try1.Properties.Settings.Default.Directory;
            string fullPath = path + "/Dossiers/" + txtBox_dossierNummer.Text;

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }

        private void btn_saveDossier_Click(object sender, RoutedEventArgs e)
        {
            string query = "Update Dossier SET Datum = '" + DateTime.Today.Date.ToString("dd-MM-yyyy") + "'";

            if (comboBox_TypeDossier.Text == "Rechtbank")
            {
                query += ", RechtbankZaaknummer = '" + txtBox_rechtbankZaaknummer.Text + "'" +
                         ", RechtbankRolRekestnummer = '" + txtBox_rechtbankRolRekestnummer.Text + "'" +
                         ", RechtbankNamenVanPartijen = '" + txtBox_rechtbankNamenVanPartijen.Text + "'";
            }
            else if (comboBox_TypeDossier.Text == "Geschillencommissie")
            {
                query += ", SGCDossierNummer = '" + txtBox_SGCDossierNummer.Text + "'" +
                         ", SGCPlaats = '" + txtBox_SGCPlaats.Text + "'";
            }

                query += " WHERE Id = " + DossierID;
            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }

            {
                MessageBox.Show("Het dossier is opgeslagen!", "Status dossier", MessageBoxButton.OK, MessageBoxImage.Information);

                win_viewDossier winViewDossier = new win_viewDossier(DossierID);
                winViewDossier.Show();
                this.Close();
            }
        }

        private void txtBox_rechtbankZaaknummer_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtBox_rechtbankRolRekestnummer_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        public void addSGC()
        {
            string query = "SELECT * FROM Dossier WHERE Dossiernummer ='" + currentDossierNummer + "'"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable idTable = new DataTable();
                adapter.Fill(idTable);

                string ID = idTable.Rows[0]["Id"].ToString();
                DossierID = int.Parse(ID);
            }

            query = "SELECT * FROM Klanten WHERE Naam ='SGC'"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)
            int SGCID;

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                string ID = dt.Rows[0]["Id"].ToString();
                SGCID = int.Parse(ID);
            }

            query = "INSERT INTO DossierKlant (DossierId, KlantId, Type) VALUES (@DossierId, @KlantId, @Type)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@DossierId", DossierID); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@KlantId", SGCID);
                command.Parameters.AddWithValue("@Type", "Aanvrager");

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            showBoundKlanten();
            
        }

        private void btn_deleteKlant_Click(object sender, RoutedEventArgs e)
        {
            if (listView_selectedKlanten.SelectedValue == null)
            {
                return;
            }
                string query = "DELETE FROM DossierKlant WHERE Id = @ID";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@ID", listView_selectedKlanten.SelectedValue);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
                showBoundKlanten();
        }
    }
}
