﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_editPrijslijst.xaml
    /// </summary>
    public partial class win_editPrijslijst : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        DataTable prijsTable = new DataTable();

        public win_editPrijslijst()
        {
            InitializeComponent();

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            populatePrijslijst();
            populateTypeComboBox();
        }

        public void populatePrijslijst()
        {
            listView_prijslijst.UnselectAll();
            string query = "SELECT * FROM Prijslijst"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                prijsTable.Clear();

                adapter.Fill(prijsTable);

                listView_prijslijst.SelectedValuePath = "Id";
                listView_prijslijst.ItemsSource = prijsTable.DefaultView;
            }
        }

        public void populateTypeComboBox()
        {
            string query = "SELECT DISTINCT Type FROM Prijslijst"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)
            DataTable items = new DataTable();

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(items);

                foreach (DataRow row in items.Rows)
                {
                    comboBox_Type.Items.Add(row["Type"].ToString()); //Add all existing types to combobox
                }
            }
        }

        private void btn_editPrijs_Click(object sender, RoutedEventArgs e)
        {
            if (listView_prijslijst.SelectedValue == null)
            {
                return;
            }

            string query = "UPDATE Prijslijst SET Item = @Item, Type = @Type, Prijs = @Prijs, Eenheid = @Eenheid, BtwTarief = @BtwTarief WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Item", txtBox_Item.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@Type", comboBox_Type.Text);
                command.Parameters.AddWithValue("@Prijs", txtBox_Prijs.Text);
                command.Parameters.AddWithValue("@Eenheid", txtBox_Eenheid.Text);
                command.Parameters.AddWithValue("@BtwTarief", txtBox_btwTarief.Text);
                command.Parameters.AddWithValue("@ID", listView_prijslijst.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populatePrijslijst();
        }

        private void listView_prijslijst_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView_prijslijst.SelectedIndex == -1)
            {
                return;
            }
            txtBox_Item.Text = prijsTable.Rows[listView_prijslijst.SelectedIndex]["Item"].ToString();
            txtBox_Prijs.Text = prijsTable.Rows[listView_prijslijst.SelectedIndex]["Prijs"].ToString();
            txtBox_Eenheid.Text = prijsTable.Rows[listView_prijslijst.SelectedIndex]["Eenheid"].ToString();
            txtBox_btwTarief.Text = prijsTable.Rows[listView_prijslijst.SelectedIndex]["BtwTarief"].ToString();
            comboBox_Type.Text = prijsTable.Rows[listView_prijslijst.SelectedIndex]["Type"].ToString();
        }

        private void btn_addPrijs_Click(object sender, RoutedEventArgs e)
        {
            string query = "INSERT INTO Prijslijst (Item, Type, Prijs, Eenheid, BtwTarief) VALUES (@Item, @Type, @Prijs, @Eenheid, @BtwTarief)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Item", txtBox_Item.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@Type", comboBox_Type.Text);
                command.Parameters.AddWithValue("@Prijs", txtBox_Prijs.Text);
                command.Parameters.AddWithValue("@Eenheid", txtBox_Eenheid.Text);
                command.Parameters.AddWithValue("@BtwTarief", txtBox_btwTarief.Text);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populatePrijslijst();
        }

        private void btn_deletePrijs_Click(object sender, RoutedEventArgs e)
        {
            if (listView_prijslijst.SelectedValue == null)
            {
                return;
            }

            string query = "DELETE FROM Prijslijst WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", listView_prijslijst.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populatePrijslijst();
        }

        private void btn_updateBtwTarief_Click(object sender, RoutedEventArgs e)
        {
            string query = "UPDATE Prijslijst SET BtwTarief = @BtwTarief";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@BtwTarief", PEB_database_try1.Properties.Settings.Default.BtwTarief);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populatePrijslijst();
        }
    }
}
