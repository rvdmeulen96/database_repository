﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_instellingen.xaml
    /// </summary>

    public class Instellingen
    {
        public string BtwTarief { get; set; }
    }

    public partial class win_instellingen : Window
    {
        public win_instellingen()
        {
            InitializeComponent();
            LoadInstellingen();
        }

       public void LoadInstellingen()
        {
            txtBox_btwTarief.Text = PEB_database_try1.Properties.Settings.Default.BtwTarief.ToString();
        }

        private void btn_applyChanges_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PEB_database_try1.Properties.Settings.Default.BtwTarief = decimal.Parse(txtBox_btwTarief.Text);
            }
            catch
            {
                MessageBox.Show("Opslaan mislukt!", "Fout opgetreden", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
    }
}
