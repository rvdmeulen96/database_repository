﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_viewKlant.xaml
    /// </summary>
    public partial class win_viewKlant : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        DataTable klantTable = new DataTable();
        DataTable dossierTable = new DataTable();
        int klantId;

        public win_viewKlant(int KlantId)
        {
            InitializeComponent();
            klantId = KlantId;

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            LoadKlant();
            populateKlantDetails();
            populateBoundDossierList();
            populateFacturen();
        }

        public void LoadKlant()
        {
            string query = "SELECT * FROM Klanten " +
                    "WHERE Id = " + klantId;

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(klantTable);
            }
        }

        public void populateKlantDetails()
        {
            txtBox_naam.Text = klantTable.Rows[0]["Naam"].ToString();
            txtBox_aanvullendeNaam.Text = klantTable.Rows[0]["AanvullendeNaam"].ToString();
            txtBox_voornaam.Text = klantTable.Rows[0]["Voornaam"].ToString();
            txtBox_tussenvoegsel.Text = klantTable.Rows[0]["Tussenvoegsel"].ToString();
            txtBox_straat.Text = klantTable.Rows[0]["Straat"].ToString();
            txtBox_huisnummer.Text = klantTable.Rows[0]["Huisnummer"].ToString();
            txtBox_Plaats.Text = klantTable.Rows[0]["Plaats"].ToString();
            txtBox_postcode.Text = klantTable.Rows[0]["Postcode"].ToString();
        }

        public void populateFacturen()
        {
            string query = "SELECT * FROM FactuurKlant WHERE KlantId = '" + klantId + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(dossierTable);

                listView_facturen.SelectedValuePath = "Id";
                listView_facturen.ItemsSource = dossierTable.DefaultView;
            }
        }

        private void btn_applyEdit_Click(object sender, RoutedEventArgs e)
        {
            string query = "UPDATE Klanten SET Naam = '" + txtBox_naam.Text + "', " +
                "AanvullendeNaam = '" + txtBox_aanvullendeNaam.Text + "', " + 
                "Voornaam = '" + txtBox_voornaam.Text +"', " +
                "Tussenvoegsel = '" + txtBox_tussenvoegsel.Text +"', " +
                "Straat = '" + txtBox_straat.Text +"', " +
                "Huisnummer = '" + txtBox_huisnummer.Text +"', " +
                "Plaats = '" + txtBox_Plaats.Text +"', " +
                "Postcode = '" + txtBox_postcode.Text +"' " +
                "WHERE Id = " + klantId;

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            LoadKlant();
            populateKlantDetails();
        }

        public void populateBoundDossierList()
        {
            string query = "SELECT Dossier.DossierNummer, DossierKlant.Type, DossierKlant.Id, DossierKlant.DossierId FROM (DossierKlant INNER JOIN Dossier ON DossierKlant.DossierId = Dossier.Id) WHERE KlantId = '" + klantId + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(dossierTable);

                listView_boundDossier.SelectedValuePath = "Id";
                listView_boundDossier.ItemsSource = dossierTable.DefaultView;
            }
        }

        private void btn_openDossier_Click(object sender, RoutedEventArgs e)
        {
            if (listView_boundDossier.SelectedValue == null)
            {
                return;
            }

            int dossierId = int.Parse(dossierTable.Rows[listView_boundDossier.SelectedIndex]["DossierId"].ToString());

            win_viewDossier winViewDossier1 = new win_viewDossier(dossierId);
            winViewDossier1.Show();
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

       
    }
}
