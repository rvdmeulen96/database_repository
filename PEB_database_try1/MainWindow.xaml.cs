﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Threading;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        

        public MainWindow()
        {
            InitializeComponent();

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            populateComboBoxDossiers();
            populateComboBoxKlanten();            
        }

        private void mainWindow_gotFocus(object sender, EventArgs e) //Laad mogelijke wijzigingen wanneer dit venster weer focus heeft
        {
            populateComboBoxDossiers();
            populateComboBoxKlanten();
        }

        private void btn_addKlant_Click(object sender, RoutedEventArgs e) //Event na click button toevoegen klant
        {
            NieuweKlant winAddKlant = new NieuweKlant();    //Nieuwe window instantieren
            winAddKlant.Show(); //Openen van aangemaakte venster
        }

        private void btn_manageKlant_Click(object sender, RoutedEventArgs e)
        {
            Win_manageKlant win_manageklant1 = new Win_manageKlant();
            win_manageklant1.Show();
        }

        private void btn_addDossier_Click(object sender, RoutedEventArgs e)
        {
            win_nieuwDossier winAddDossier =new win_nieuwDossier();
            winAddDossier.Show();
        }

        private void btn_editPrijslijst(object sender, RoutedEventArgs e)
        {
            win_editPrijslijst win_editPrijslijst1 = new win_editPrijslijst();
            win_editPrijslijst1.Show();
        }

        private void btn_manageDossier_Click(object sender, RoutedEventArgs e)
        {
            win_manageDossier win_manageDossier1 = new win_manageDossier();
            win_manageDossier1.Show();
        }

        public void populateComboBoxDossiers()
        {
            string query = "SELECT * FROM Dossier ORDER BY DossierNummer"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable dossiersTable = new DataTable();
                dossiersTable.Clear();

                adapter.Fill(dossiersTable);

                comboBox_huidigDossier.SelectedValuePath = "Id";
                comboBox_huidigDossier.ItemsSource = dossiersTable.DefaultView;
            }
        }

        public void populateComboBoxKlanten()
        {
            string query = "SELECT * FROM Klanten ORDER BY Naam"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable klantenTable = new DataTable();
                klantenTable.Clear();

                adapter.Fill(klantenTable);

                comboBox_klant.SelectedValuePath = "Id";
                comboBox_klant.ItemsSource = klantenTable.DefaultView;
            }
        }

        private void txtBox_zoekKlant_TextChanged(object sender, TextChangedEventArgs e)
        {
            string query = "SELECT * FROM Klanten WHERE Naam LIKE '" + txtBox_zoekKlant.Text + "%' ORDER BY Naam"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable klantenTable = new DataTable();
                klantenTable.Clear();

                adapter.Fill(klantenTable);

                comboBox_klant.SelectedValuePath = "Id";
                comboBox_klant.ItemsSource = klantenTable.DefaultView;
            }
            comboBox_klant.IsDropDownOpen = true;
        }

        private void btn_openDossier_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                win_viewDossier winViewDossier1 = new win_viewDossier(int.Parse(comboBox_huidigDossier.SelectedValue.ToString()));
                winViewDossier1.Show();
            }
            catch { }
        }

        private void btn_openKlant_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                win_viewKlant winViewKlant1 = new win_viewKlant(int.Parse(comboBox_klant.SelectedValue.ToString()));
                winViewKlant1.Show();
            }
            catch { }
        }

        private void txtBox_zoekDossier_TextChanged(object sender, TextChangedEventArgs e)
        {
            string query = "SELECT * FROM Dossier WHERE DossierNummer LIKE '" + txtBox_zoekDossier.Text + "%' ORDER BY DossierNummer"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable dossiersTable = new DataTable();
                dossiersTable.Clear();

                adapter.Fill(dossiersTable);

                comboBox_huidigDossier.SelectedValuePath = "Id";
                comboBox_huidigDossier.ItemsSource = dossiersTable.DefaultView;
            }
            comboBox_huidigDossier.IsDropDownOpen = true;
        }

        private void btn_editInstellingen(object sender, RoutedEventArgs e)
        {
            win_instellingen winInstellingen = new win_instellingen();
            winInstellingen.Show();
        }
    }
}
