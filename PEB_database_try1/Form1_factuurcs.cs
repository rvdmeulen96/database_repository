﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Reporting.WinForms;
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;

namespace PEB_database_try1
{
    public partial class Form1_factuurcs : Form
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        int factuurId = 0;
        int partijNr = 1;

        public Form1_factuurcs(int FACTUURID, int partijNR)
        {
            InitializeComponent();
            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;
            factuurId = FACTUURID;
            partijNr = partijNR;

            if (partijNr == 1)
            {
                try
                {
                    makeFactuurPartij1();
                }
                catch
                { }
            }
            else if (partijNr == 2)
            {
                try
                {
                    
                }
                catch
                { }
            }

        }

        private void Form1_factuurcs_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'Database_PEBDataSet.DataTable1' table. You can move, or remove it, as needed.
            //this.DataTable1TableAdapter.Fill(this.Database_PEBDataSet.DataTable1);

            //this.reportViewer1.RefreshReport();
        }

        public void makeFactuurPartij1()
        {
            //Selecteer data voor totaaltelling werk op factuur
            string query = "SELECT Werk.Id, Werk.Werk, Werk.Aantal, Werk.PrijsPerEenheid, Werk.Eenheid, Werk.PrijsTotaal, Werk.PrijsTotaalIncl, Werk.BtwTarief, Werk.DossierId, Werk.Omschrijving, FactuurWerk.PercPartij1, FactuurWerk.PercPartij2, " +
                         "FactuurWerk.PrijsPartij1, FactuurWerk.PrijsPartij2 FROM " +
                         "FactuurWerk INNER JOIN Werk ON FactuurWerk.WerkId = Werk.Id " +
                         "WHERE FactuurWerk.FactuurId = '" + factuurId + "'";
            DataTable dt = new DataTable();

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                dt.Clear();

                adapter.Fill(dt);
            }

            //Gegevens partijen:
            query = "SELECT Klanten.* FROM FactuurWerk INNER JOIN Klanten ON FactuurWerk.KlantIdPartij1 = Klanten.Id " +
                         "WHERE FactuurWerk.FactuurId = '" + factuurId + "'";
            DataTable dtPartij1 = new DataTable();

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                dtPartij1.Clear();

                adapter.Fill(dtPartij1);
            }

            //Deel 1 parameters
            ReportParameter[] parameters = new ReportParameter[18];
            parameters[0] = new ReportParameter("Naam_partij1", dtPartij1.Rows[0]["Naam"].ToString());
            parameters[1] = new ReportParameter("Straat_partij1", dtPartij1.Rows[0]["Straat"].ToString());
            parameters[2] = new ReportParameter("Huisnummer_partij1", dtPartij1.Rows[0]["Huisnummer"].ToString());
            parameters[3] = new ReportParameter("Postcode_partij1", dtPartij1.Rows[0]["Postcode"].ToString());
            parameters[4] = new ReportParameter("Plaats_partij1", dtPartij1.Rows[0]["Plaats"].ToString());
            parameters[5] = new ReportParameter("Postbus_partij1", dtPartij1.Rows[0]["Postbus"].ToString());
            parameters[6] = new ReportParameter("PostcodePostbus_partij1", dtPartij1.Rows[0]["PostcodePostbus"].ToString());
            parameters[7] = new ReportParameter("PlaatsPostbus_partij1", dtPartij1.Rows[0]["PlaatsPostbus"].ToString());

            //Zoek partij 2:
            try //Probeer of partij 2 bestaat:
            {
                query = "SELECT Klanten.* FROM FactuurWerk INNER JOIN Klanten ON FactuurWerk.KlantIdPartij2 = Klanten.Id " +
                         "WHERE FactuurWerk.FactuurId = '" + factuurId + "'";
                DataTable dtPartij2 = new DataTable();

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    dtPartij2.Clear();

                    adapter.Fill(dtPartij2);
                }

                parameters[8] = new ReportParameter("Naam_partij2", dtPartij2.Rows[0]["Naam"].ToString());
                parameters[9] = new ReportParameter("Straat_partij2", dtPartij2.Rows[0]["Straat"].ToString());
                parameters[10] = new ReportParameter("Huisnummer_partij2", dtPartij2.Rows[0]["Huisnummer"].ToString());
                parameters[11] = new ReportParameter("Postcode_partij2", dtPartij2.Rows[0]["Postcode"].ToString());
                parameters[12] = new ReportParameter("Plaats_partij2", dtPartij2.Rows[0]["Plaats"].ToString());
                parameters[13] = new ReportParameter("Postbus_partij2", dtPartij2.Rows[0]["Postbus"].ToString());
                parameters[14] = new ReportParameter("PostcodePostbus_partij2", dtPartij2.Rows[0]["PostcodePostbus"].ToString());
                parameters[15] = new ReportParameter("PlaatsPostbus_partij2", dtPartij2.Rows[0]["PlaatsPostbus"].ToString());
                parameters[16] = new ReportParameter("exists_partij2", "true");
            }
            catch //Anders niet invullen:
            {
                
                parameters[8] = new ReportParameter("Naam_partij2", "");
                parameters[9] = new ReportParameter("Straat_partij2","");
                parameters[10] = new ReportParameter("Huisnummer_partij2", "");
                parameters[11] = new ReportParameter("Postcode_partij2", "");
                parameters[12] = new ReportParameter("Plaats_partij2", "");
                parameters[13] = new ReportParameter("Postbus_partij2", "");
                parameters[14] = new ReportParameter("PostcodePostbus_partij2", "");
                parameters[15] = new ReportParameter("PlaatsPostbus_partij2", "");
                parameters[16] = new ReportParameter("exists_partij2", "false");
            }

            //Totaalbedrag factuur en factuurnummer
            try
            {
                query = "SELECT * FROM FactuurKlant WHERE FactuurId = '" + factuurId + "'";
                DataTable dtFactuurKlant = new DataTable();

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    dtFactuurKlant.Clear();

                    adapter.Fill(dtFactuurKlant);
                }

                parameters[17] = new ReportParameter("Factuurnummer", dtFactuurKlant.Rows[0]["Factuurnummer"].ToString());
            }
            catch
            {
                parameters[17] = new ReportParameter("Factuurnummer","Not found!");
            }

            this.reportViewer1.LocalReport.SetParameters(parameters);

            ReportDataSource rds = new ReportDataSource("DataSet2", dt);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            //this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }
    }
}
