﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_manageDossier.xaml
    /// </summary>
    public partial class win_manageDossier : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        public win_manageDossier()
        {
            InitializeComponent();

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            populateComboboxFilter();
            populateDossierList();
        }

        public void populateComboboxFilter()
        {
            comboBox_filter.Items.Add("Alles");
            comboBox_filter.Items.Add("Dossiernummer");
            comboBox_filter.Items.Add("Soort dossier");
            comboBox_filter.SelectedIndex = 0;
        }

        public void populateDossierList()
        {
            try
            {
                string query = "SELECT * FROM Dossier"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

                if (comboBox_filter.Text == "Dossiernummer")
                {
                    query += " WHERE DossierNummer LIKE '" + txtBox_zoeken.Text + "%'"; 
                }
                else if (comboBox_filter.Text == "Soort dossier")
                {
                    query += " WHERE DossierSoort LIKE '" + txtBox_zoeken.Text + "%'";
                }

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    DataTable dossierTable = new DataTable();
                    adapter.Fill(dossierTable);

                    listView_dossierList.SelectedValuePath = "Id";
                    listView_dossierList.ItemsSource = dossierTable.DefaultView;
                }
            }
            catch { }
            
        }

        private void btn_openDossier_Click(object sender, RoutedEventArgs e)
        {
            if (listView_dossierList.SelectedValue == null)
            {
                return;
            }

            win_viewDossier winViewDossier1 = new win_viewDossier(int.Parse(listView_dossierList.SelectedValue.ToString()));
            winViewDossier1.Show();
        }

        private void btn_deleteDossier_Click(object sender, RoutedEventArgs e)
        {
            if (listView_dossierList.SelectedValue == null)
            {
                return;
            }

            MessageBoxResult result = MessageBox.Show("Weet u zeker dat u het geselecteerde dossier wilt verwijderen?", "Verwijderen dossier", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }

            string query = "DELETE FROM Dossier WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", listView_dossierList.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateDossierList();
        }

        private void btn_addDossier_Click(object sender, RoutedEventArgs e)
        {
            win_nieuwDossier winAddDossier = new win_nieuwDossier();
            winAddDossier.Show();
        }

        private void txtBox_zoeken_Changed(object sender, TextChangedEventArgs e)
        {
            populateDossierList();
        }

        private void comboBox_filter_Closed(object sender, EventArgs e)
        {
            populateDossierList();

            if (comboBox_filter.Text == "Dossiernummer")
            {
                txtBox_zoeken.Text = "PEB";

                Keyboard.Focus(txtBox_zoeken);
                txtBox_zoeken.Select(txtBox_zoeken.Text.Length, 0);
            }
            else
            {
                Keyboard.Focus(txtBox_zoeken);
                txtBox_zoeken.SelectAll();
            }
        }
    }
}
