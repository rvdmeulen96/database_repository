﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.Win32;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for win_viewDossier.xaml
    /// </summary>
    public partial class win_viewDossier : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        int dossierId;
        int werkId = 0;
        DataTable klantTable = new DataTable();
        DataTable dossierTable = new DataTable();
        DataTable prijsTable = new DataTable();
        DataTable werkTable = new DataTable();
        DataTable werkTableFactuur = new DataTable();
        DataTable usedWerkTable = new DataTable();
        DataTable factuurTable = new DataTable();
        DataTable werkPartijenTable = new DataTable();

        public win_viewDossier(int DossierId)
        {
            InitializeComponent();
            dossierId = DossierId;

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            populateInitial();
        }

        //-----------------------------------------------------------------------------------------------------------
        //Populate elements:

        //Comboboxes:
        public void populateComboBoxFilterWerk()
        {
            string query = "SELECT DISTINCT Type FROM Prijslijst ORDER BY Type"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable filterWerkTable = new DataTable();
                filterWerkTable.Clear();

                adapter.Fill(filterWerkTable);

                comboBox_filterWerk.Items.Add("Alles");
                foreach (DataRow row in filterWerkTable.Rows)
                {
                    comboBox_filterWerk.Items.Add(row["Type"].ToString());
                }
            }
        }

        public void populateComboBoxDossiers()
        {
            string query = "SELECT * FROM Dossier ORDER BY DossierNummer"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable dossiersTable = new DataTable();
                dossiersTable.Clear();

                adapter.Fill(dossiersTable);

                comboBox_huidigDossier.SelectedValuePath = "Id";
                comboBox_huidigDossier.ItemsSource = dossiersTable.DefaultView;
            }
            comboBox_huidigDossier.SelectedValue = dossierId;
        }

        public void populateComboBoxPrijslijst()
        {
            string query = "";
            try
            {
                if (comboBox_filterWerk.SelectedIndex == 0)
                {
                    query = "SELECT * FROM Prijslijst ORDER BY Item"; //Query die je operatie definieert
                }
                else
                {
                    query = "SELECT * FROM Prijslijst WHERE Type = '" + comboBox_filterWerk.Text + "' ORDER BY Item";
                }
            }
            catch
            {

            }

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                prijsTable.Clear();

                adapter.Fill(prijsTable);

                comboBox_prijslijst.SelectedValuePath = "Id";
                comboBox_prijslijst.ItemsSource = prijsTable.DefaultView;
            }
        }

        public void populateComboBoxFacturen()
        {
            string query = "SELECT * FROM Factuur WHERE DossierId = '" + dossierId + "' ORDER BY Datum";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                factuurTable.Clear();
                adapter.Fill(factuurTable);

                comboBox_facturen.SelectedValuePath = "Id";
                comboBox_facturen.ItemsSource = factuurTable.DefaultView;

                try
                {
                    comboBox_facturen.SelectedIndex = 0;
                }
                catch { }
            }
        }

        //Listviews:
        public void showBoundKlanten()
        {
            string query = "SELECT Klanten.Naam, Klanten.Straat, Klanten.Huisnummer, Klanten.Plaats, DossierKlant.Type, DossierKlant.KlantId, DossierKlant.Id, DossierKlant.PartijNummer FROM (DossierKlant INNER JOIN Klanten ON DossierKlant.KlantId = Klanten.Id) WHERE DossierId = '" + dossierId + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(klantTable);

                listView_selectedKlanten.SelectedValuePath = "Id";
                listView_selectedKlanten.ItemsSource = klantTable.DefaultView;
            }
        }

        public void populateWerkList()
        {
            string query = "SELECT * FROM Werk WHERE DossierId = '" + dossierId + "' ORDER BY Werk"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                werkTable.Clear();
                adapter.Fill(werkTable);

                listView_werk.SelectedValuePath = "Id";
                listView_werk.ItemsSource = werkTable.DefaultView;
            }
        }

        public void populateAllWerk()
        {
            try
            {
                string query = "";
                if (radioBtn_filterNotAdded.IsChecked == true)
                {
                    query = "SELECT * FROM Werk WHERE Werk.Id NOT IN (SELECT WerkId FROM FactuurWerk WHERE FactuurWerk.FactuurId = '" + comboBox_facturen.SelectedValue + "') AND Werk.DossierId = '" + dossierId + "' ORDER BY Werk.Werk";
                    //query = "SELECT * FROM Werk a WHERE NOT EXISTS ( SELECT * FROM FactuurWerk b WHERE b.WerkId = a.Id)";
                }
                else
                {
                    query = "SELECT * FROM Werk WHERE DossierId = '" + dossierId + "' ORDER BY Werk";
                }

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    werkTableFactuur.Clear();
                    adapter.Fill(werkTableFactuur);

                    listView_allWerk.SelectedValuePath = "Id";
                    listView_allWerk.ItemsSource = werkTableFactuur.DefaultView;
                }
            }
            catch
            {
            }
        }

        public void populateAddedWerk()
        {
            if (comboBox_facturen.SelectedValue == null)
            {
                return;
            }
            string query = "SELECT FactuurWerk.Id, Werk.Werk, Werk.Aantal, Werk.PrijsPerEenheid, Werk.Eenheid, Werk.PrijsTotaal, Werk.BtwTarief FROM (FactuurWerk INNER JOIN Werk ON FactuurWerk.WerkId = Werk.Id) WHERE FactuurId = '" + comboBox_facturen.SelectedValue + "' ORDER BY Werk";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                usedWerkTable.Clear();
                adapter.Fill(usedWerkTable);

                listView_addedWerk.SelectedValuePath = "Id";
                listView_addedWerk.ItemsSource = usedWerkTable.DefaultView;
            }
        }

        public void populateListViewAllKlanten()
        {
            string query = "SELECT Klanten.Naam, Klanten.Straat, Klanten.Huisnummer, Klanten.Plaats, DossierKlant.Type, DossierKlant.KlantId, DossierKlant.Id, DossierKlant.PartijNummer FROM (DossierKlant INNER JOIN Klanten ON DossierKlant.KlantId = Klanten.Id) WHERE DossierId = '" + dossierId + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable klantenTable = new DataTable();
                adapter.Fill(klantenTable);

                listView_AllKlanten.SelectedValuePath = "Id";
                listView_AllKlanten.ItemsSource = klantenTable.DefaultView;
            }
            populateListViewPartij1();
            populateListViewPartij2();
            populateWerkPartijen();
        }

        public void populateListViewPartij1()
        {
            string query = "SELECT Klanten.Naam, Klanten.Straat, Klanten.Huisnummer, Klanten.Plaats, DossierKlant.Type, DossierKlant.KlantId, DossierKlant.Id, DossierKlant.PartijNummer FROM (DossierKlant INNER JOIN Klanten ON DossierKlant.KlantId = Klanten.Id) WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 1'";
            DataTable partij1Table = new DataTable();

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(partij1Table);

                listView_Partij1.SelectedValuePath = "Id";
                listView_Partij1.ItemsSource = partij1Table.DefaultView;

                listView_Partij12.SelectedValuePath = "Id";
                listView_Partij12.ItemsSource = partij1Table.DefaultView;
            }

            try
            {
                query = "UPDATE FactuurWerk SET KlantIdPartij1 = @KlantId WHERE DossierId = @Id";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@Id", dossierId);
                    command.Parameters.AddWithValue("@KlantId", partij1Table.Rows[0]["KlantId"]);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch
            {
            }
        }

        public void populateListViewPartij2()
        {
            string query = "SELECT Klanten.Naam, Klanten.Straat, Klanten.Huisnummer, Klanten.Plaats, DossierKlant.Type, DossierKlant.KlantId, DossierKlant.Id, DossierKlant.PartijNummer FROM (DossierKlant INNER JOIN Klanten ON DossierKlant.KlantId = Klanten.Id) WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 2'";
            DataTable partij2Table = new DataTable();

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                adapter.Fill(partij2Table);

                listView_Partij2.SelectedValuePath = "Id";
                listView_Partij2.ItemsSource = partij2Table.DefaultView;

                listView_Partij22.SelectedValuePath = "Id";
                listView_Partij22.ItemsSource = partij2Table.DefaultView;
            }


            try
            {
                query = "UPDATE FactuurWerk SET KlantIdPartij1 = @KlantId WHERE DossierId = @Id";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@Id", dossierId);
                    command.Parameters.AddWithValue("@KlantId", partij2Table.Rows[0]["KlantId"]);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch
            { }
        }

        public void populateWerkPartijen()
        {
            string query = "SELECT w.Id, fw.PercPartij1, fw.PercPartij2, fw.PrijsPartij1, fw.PrijsPartij2, fw.KlantIdPartij1, fw.KlantIdPartij2, w.Werk, w.PrijsTotaal, f.TotaalBedragExBtw, fw.FactuurId FROM FactuurWerk fw " +
                "INNER JOIN Werk w ON fw.WerkId = w.Id " +
                "INNER JOIN Factuur f ON fw.FactuurId = f.Id " +
                "WHERE fw.FactuurId = '" + comboBox_facturen.SelectedValue + "'";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                werkPartijenTable.Clear();
                adapter.Fill(werkPartijenTable);

                listView_werkPartij1.SelectedValuePath = "Id";
                listView_werkPartij1.ItemsSource = werkPartijenTable.DefaultView;
            }
            try
            {
                txtBox_totPrijsTotaalPartij1.Text = werkPartijenTable.Rows[0]["TotaalBedragExBtw"].ToString();
            }
            catch { }
        }

        //Overige:
        private void populateDossierDetails()
        {
            txtBox_dossierNummer.Text = dossierTable.Rows[0]["DossierNummer"].ToString();
            txtBox_datum.Text = dossierTable.Rows[0]["Datum"].ToString();
            comboBox_dossierSoort.Text = dossierTable.Rows[0]["DossierSoort"].ToString();

            if (comboBox_dossierSoort.Text == "Rechtbank")
            {
                txtBox_zaaknummer.Text = dossierTable.Rows[0]["RechtbankZaaknummer"].ToString();
                txtBox_rolRekestnummer.Text = dossierTable.Rows[0]["RechtbankRolRekestnummer"].ToString();
                txtBox_namenVanPartijen.Text = dossierTable.Rows[0]["RechtbankNamenVanPartijen"].ToString();
            }
            else if (comboBox_dossierSoort.Text == "Geschillencommissie")
            {
                txtBox_SGCdossierNummer.Text = dossierTable.Rows[0]["SGCDossierNummer"].ToString();
                txtBox_SGCplaats.Text = dossierTable.Rows[0]["SGCPlaats"].ToString();
            }

            visibilityGrids();
        }

        public void populateFactuurDetails()
        {
            if (comboBox_facturen.SelectedValue == null)
            {
                return;
            }
            DatePicker_factuurDatum.Text = factuurTable.Rows[comboBox_facturen.SelectedIndex]["Datum"].ToString();
            txtBox_factuurStatus.Text = factuurTable.Rows[comboBox_facturen.SelectedIndex]["Status"].ToString();
        }

        public void loadDossier()
        {
            string query = "SELECT * FROM Dossier " +
                    "WHERE Id = " + dossierId + "ORDER BY DossierNummer";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                dossierTable.Clear();
                adapter.Fill(dossierTable);
            }
        }

        public void setTotaalBedragFactuur()
        {
            try
            {
                string query = "SELECT TotaalBedragExBtw FROM Factuur WHERE Id = '" + comboBox_facturen.SelectedValue + "'";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    DataTable totaalBedragFactuur = new DataTable();
                    adapter.Fill(totaalBedragFactuur);

                    txtBox_totaalBedragFactuur.Text = decimal.Parse(totaalBedragFactuur.Rows[0]["TotaalBedragExBtw"].ToString()).ToString("c2");
                }
            }
            catch { }
        }

        //Combinaties van bovenstaande functies:

        public void populateComboboxes()
        {
            populateComboBoxDossiers();
            populateComboBoxFacturen();
            populateComboBoxFilterWerk();
            populateComboBoxPrijslijst();
        }

        public void populateWerkzaamheden()
        {
            populateWerkList();
            populateAddedWerk();
            populateAllWerk();
            populateListViewPartij1();
            populateListViewPartij2();
            populateWerkPartijen();
            calculateTotalenFactuur();
        }

        public void populateInitial()
        {
            loadDossier();
            populateComboboxes();
            populateDossierDetails();
            populateListViewAllKlanten();
            showBoundKlanten();
            populateFactuurDetails();
            populateWerkzaamheden();
            setTotaalBedragFactuur();
        }

        //-----------------------------------------------------------------------------------------------------------------
        // Gedefinieerde functies:

        //Berekeningen:
        public void calculatePrijs(string trigger)
        {
            try
            {
                decimal unitPrijs = 0;
                decimal aantal = 0;
                decimal btw = 0;
                decimal totaalPrijs = 0;
                decimal totaalPrijsIncl = 0;
                string toCalculate = "";

                //Bepaal wat berekend moet worden:
                if (trigger == "TotaalPrijs_Changed" && checkBox_aantal.IsChecked == false)
                {
                    toCalculate = "Aantal_TotaalIncl";
                }
                else if (trigger == "TotaalPrijs_Changed" && checkBox_aantal.IsChecked == true)
                {
                    toCalculate = "Prijs_TotaalIncl";
                }
                else if (trigger == "TotaalPrijsIncl_Changed" && checkBox_aantal.IsChecked == false)
                {
                    toCalculate = "Aantal_Totaal";
                }
                else if (trigger == "TotaalPrijsIncl_Changed" && checkBox_aantal.IsChecked == true)
                {
                    toCalculate = "Prijs_Totaal";
                }
                else if (trigger == "Aantal_Changed" && checkBox_totaalprijs.IsChecked == false)
                {
                    toCalculate = "Totaal_TotaalIncl";
                }
                else if (trigger == "Aantal_Changed" && checkBox_totaalprijs.IsChecked == true)
                {
                    toCalculate = "Prijs";
                }

                //Voer de berekening uit:
                if (toCalculate == "Aantal_TotaalIncl")
                {
                    unitPrijs = decimal.Parse(txtBox_Prijs.Text);
                    totaalPrijs = decimal.Parse(txtBox_prijsTotaal.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    aantal = totaalPrijs / unitPrijs;
                    totaalPrijsIncl = totaalPrijs * btw;
                }
                else if (toCalculate == "Prijs_TotaalIncl")
                {
                    aantal = decimal.Parse(txtBox_Aantal.Text);
                    totaalPrijs = decimal.Parse(txtBox_prijsTotaal.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    unitPrijs = totaalPrijs / aantal;
                    totaalPrijsIncl = totaalPrijs * btw;
                }
                else if (toCalculate == "Aantal_Totaal")
                {
                    unitPrijs = decimal.Parse(txtBox_Prijs.Text);
                    totaalPrijsIncl = decimal.Parse(txtBox_prijsTotaalIncl.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    totaalPrijs = totaalPrijsIncl / btw;
                    aantal = totaalPrijs / unitPrijs;
                }
                else if (toCalculate == "Prijs_Totaal")
                {
                    aantal = decimal.Parse(txtBox_Aantal.Text);
                    totaalPrijsIncl = decimal.Parse(txtBox_prijsTotaalIncl.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    totaalPrijs = totaalPrijsIncl / btw;
                    unitPrijs = totaalPrijs / aantal;
                }
                else if (toCalculate == "Totaal_TotaalIncl")
                {
                    unitPrijs = decimal.Parse(txtBox_Prijs.Text);
                    aantal = decimal.Parse(txtBox_Aantal.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    totaalPrijs = unitPrijs * aantal;
                    totaalPrijsIncl = totaalPrijs * btw;
                }
                else if (toCalculate == "Prijs")
                {
                    totaalPrijs = decimal.Parse(txtBox_prijsTotaal.Text);
                    aantal = decimal.Parse(txtBox_Aantal.Text);
                    btw = decimal.Parse(txtBox_btwTarief.Text);
                    unitPrijs = totaalPrijs / aantal;
                    totaalPrijsIncl = totaalPrijs * btw;
                }

                //Uitvoer resultaten in textboxes:
                if (!txtBox_Prijs.IsKeyboardFocused)
                {
                    txtBox_Prijs.Text = unitPrijs.ToString("F");
                }
                if (!txtBox_Aantal.IsKeyboardFocused)
                {
                    txtBox_Aantal.Text = aantal.ToString("F");
                }
                if (!txtBox_prijsTotaal.IsKeyboardFocused)
                {
                    txtBox_prijsTotaal.Text = totaalPrijs.ToString("F");
                }
                if (!txtBox_prijsTotaalIncl.IsKeyboardFocused)
                {
                    txtBox_prijsTotaalIncl.Text = totaalPrijsIncl.ToString("F");
                }
            }
            catch
            {
                return;
            }
        }

        public void calculateTotalenFactuur()
        {
            DataTable allWerkTable = new DataTable();

            try
            {
                //Ophalen van al het werk voor deze factuur
                string query = "SELECT a.Id, a.FactuurId, w.PrijsTotaal, w.PrijsTotaalIncl, w.BtwTarief " +
                    "FROM FactuurWerk a INNER JOIN Werk w ON a.WerkId = w.Id " +
                    "WHERE FactuurId = '" + comboBox_facturen.SelectedValue + "'";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    adapter.Fill(allWerkTable); //Load all werk
                }

                //Berekeningen uitvoeren:
                decimal totalExBtw = 0;
                decimal totalInclBtw = 0;
                foreach (DataRow row in allWerkTable.Rows)
                {
                    try
                    {
                        totalExBtw += decimal.Parse(row["PrijsTotaal"].ToString());
                        totalInclBtw += decimal.Parse(row["BtwTarief"].ToString());
                    }
                    catch
                    {
                        MessageBox.Show("Berekening totalen mislukt, een van de bedragen van werkzaamheden niet herkend!", "Totaaltelling mislukt!", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }

                query = "UPDATE Factuur SET TotaalBedragExBtw = @TotaalBedragExBtw, TotaalBedragInclBtw = @TotaalBedragInclBtw WHERE Id = @Id";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@TotaalBedragExBtw", totalExBtw); //Parameters toewijzen, in dit geval tekst uit textbox
                    command.Parameters.AddWithValue("@TotaalBedragInclBtw", totalInclBtw);
                    command.Parameters.AddWithValue("@ID", comboBox_facturen.SelectedValue);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch { setTotaalBedragFactuur(); }

            setTotaalBedragFactuur();
        }


        //Overige:
        public void visibilityGrids()
        {
            string selection = comboBox_dossierSoort.Text.ToString();

            if (selection == "Rechtbank")
            {
                Grid_Rechtbank.Visibility = Visibility.Visible;
                Grid_SGC.Visibility = Visibility.Collapsed;
            }
            else if (selection == "Geschillencommissie")
            {
                Grid_Rechtbank.Visibility = Visibility.Collapsed;
                Grid_SGC.Visibility = Visibility.Visible;
            }
            else
            {
                Grid_Rechtbank.Visibility = Visibility.Collapsed;
                Grid_SGC.Visibility = Visibility.Collapsed;
            }
        }

        public string generateFactuurnummer()
        {
            string query = "SELECT DISTINCT Factuurnummer FROM FactuurKlant ORDER BY Factuurnummer";
            string newFactuurnummer = "";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                DataTable factuurnummersTable = new DataTable();
                adapter.Fill(factuurnummersTable);

                try
                {
                    newFactuurnummer = "PEB" + (int.Parse(factuurnummersTable.Rows[factuurnummersTable.Rows.Count - 1]["Factuurnummer"].ToString().Remove(0, 3)) + 1).ToString();
                }
                catch
                {
                    return ("PEB100");
                }
            }
            return ("PEB" + newFactuurnummer.ToString());
        }

        public void createFactuurKlant()
        {
            //try
            //{
            decimal totalPartij1 = decimal.Parse(werkPartijenTable.Compute("Sum(PrijsPartij1)", "").ToString());
            decimal totalPartij2 = decimal.Parse(werkPartijenTable.Compute("Sum(PrijsPartij2)", "").ToString());

            if (totalPartij1 > 0) //De klant moet betalen -> factuur aanmaken/update
            {
                //factuur updaten (mits deze al bestaat)
                string query = "IF EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "UPDATE FactuurKlant SET FactuurId = @FactuurId, DossierId = @DossierId, KlantId = @KlantId, Factuurnummer = @Factuurnummer " +
                "WHERE FactuurId = @FactuurId AND KlantId = @KlantId";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    string factuurNummer = generateFactuurnummer();

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@DossierId", dossierId);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij1"]);
                    command.Parameters.AddWithValue("@Factuurnummer", factuurNummer);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }

                //factuur aanmaken (als de factuur nog niet bestaat)
                query = "IF NOT EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "INSERT INTO FactuurKlant (FactuurId, DossierId, KlantId, Factuurnummer) VALUES (@FactuurId, @DossierId, @KlantId, @Factuurnummer)";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    string factuurNummer = generateFactuurnummer();

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@DossierId", dossierId);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij1"]);
                    command.Parameters.AddWithValue("@Factuurnummer", factuurNummer);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            else //De klant hoeft niet te betalen -> eventuele aangemaakte factuur verwijderen
            {
                string query = "IF EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "DELETE FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij1"]);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }

            if (totalPartij2 > 0) //De klant moet betalen -> factuur aanmaken
            {
                //Update factuur als deze bestaat
                string query = "IF EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "UPDATE FactuurKlant SET FactuurId = @FactuurId, DossierId = @DossierId, KlantId = @KlantId, Factuurnummer= @Factuurnummer " +
                "WHERE FactuurId = @FactuurId AND KlantId = @KlantId";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    string factuurNummer = generateFactuurnummer();

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@DossierId", dossierId);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij2"]);
                    command.Parameters.AddWithValue("@Factuurnummer", factuurNummer);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }

                //Nieuwe factuur als deze nog niet bestaat
                query = "IF NOT EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "INSERT INTO FactuurKlant (FactuurId, DossierId, KlantId, Factuurnummer) VALUES (@FactuurId, @DossierId, @KlantId, @Factuurnummer)";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    string factuurNummer = generateFactuurnummer();

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@DossierId", dossierId);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij2"]);
                    command.Parameters.AddWithValue("@Factuurnummer", factuurNummer);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            else //De klant hoeft niet te betalen -> eventuele aangemaakte factuur verwijderen
            {
                string query = "IF EXISTS (SELECT Id FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId) " +
                "DELETE FROM FactuurKlant WHERE FactuurId = @FactuurId AND KlantId = @KlantId";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@FactuurId", werkPartijenTable.Rows[0]["FactuurId"]);
                    command.Parameters.AddWithValue("@KlantId", werkPartijenTable.Rows[0]["KlantIdPartij2"]);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            //}
            //catch
            //{
            //    MessageBox.Show("error");
            //}
        }

        //-----------------------------------------------------------------------------------------------------------------
        // Acties gesorteerd per tabblad:

        //========
        //Algemeen:
        //========
        //Comboboxes
        private void comboBox_huidigDossier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                dossierId = int.Parse(comboBox_huidigDossier.SelectedValue.ToString());
            }
            catch
            {
                return;
            }
            populateInitial();
        }


        //==================
        //Dossier gegevens:
        //==================
        //Buttons
        private void btn_editDossier_Click(object sender, RoutedEventArgs e)
        {
            string query = "UPDATE Dossier SET DossierNummer = '" + txtBox_dossierNummer.Text + "', " +
                "Datum = '" + txtBox_datum.Text + "', " +
                "DossierSoort = '" + comboBox_dossierSoort.Text + "', ";
            if (comboBox_dossierSoort.Text == "Rechtbank" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Rechtbank")
            {
                query = query + "RechtbankZaaknummer = '" + txtBox_zaaknummer.Text + "', " +
                "RechtbankRolRekestnummer = '" + txtBox_rolRekestnummer.Text + "', " +
                "RechtbankNamenVanPartijen = '" + txtBox_namenVanPartijen.Text + "' ";
            }
            else if (comboBox_dossierSoort.Text == "Rechtbank" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Geschillencommissie")
            {
                query = query + "RechtbankZaaknummer = '" + txtBox_zaaknummer.Text + "', " +
                "RechtbankRolRekestnummer = '" + txtBox_rolRekestnummer.Text + "', " +
                "RechtbankNamenVanPartijen = '" + txtBox_namenVanPartijen.Text + "', " +
                "SGCDossierNummer = '', " +
                "SGCPlaats = '' ";
            }
            else if (comboBox_dossierSoort.Text == "Geschillencommissie" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Geschillencommissie")
            {
                query = query + "SGCDossierNummer = '" + txtBox_SGCdossierNummer.Text + "', " +
                "SGCPlaats = '" + txtBox_SGCplaats.Text + "' ";
            }
            else if (comboBox_dossierSoort.Text == "Geschillencommissie" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Rechtbank")
            {
                query = query + "RechtbankZaaknummer = '', " +
                "RechtbankRolRekestnummer = '', " +
                "RechtbankNamenVanPartijen = '', " +
                "SGCDossierNummer = '" + txtBox_SGCdossierNummer.Text + "', " +
                "SGCPlaats = '" + txtBox_SGCplaats.Text + "' ";
            }
            else if (comboBox_dossierSoort.Text == "PEB" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Rechtbank")
            {
                query = query + "RechtbankZaaknummer = '', " +
                "RechtbankRolRekestnummer = '', " +
                "RechtbankNamenVanPartijen = '' ";
            }
            else if (comboBox_dossierSoort.Text == "PEB" && dossierTable.Rows[0]["DossierSoort"].ToString() == "Geschillencommissie")
            {
                query = query + "SGCDossierNummer = '', " +
                "SGCPlaats = '' ";
            }
            query = query + "WHERE Id = " + dossierId;

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            loadDossier();
            populateDossierDetails();
        }

        //Comboboxes
        private void comboBox_dossierSoort_Changed(object sender, EventArgs e)
        {
            visibilityGrids();
        }


        //=======
        //Klanten
        //=======
        //Buttons
        private void btn_viewKlant_Click(object sender, RoutedEventArgs e)
        {
            if (listView_selectedKlanten.SelectedValue == null)
            {
                return;
            }

            int klantId = int.Parse(klantTable.Rows[listView_selectedKlanten.SelectedIndex]["KlantId"].ToString());

            win_viewKlant winViewKlant2 = new win_viewKlant(klantId);
            winViewKlant2.Show();
        }


        //=============
        //Verricht werk
        //=============
        //Buttons
        private void btn_deleteWerk_Click(object sender, RoutedEventArgs e)
        {
            //Verwijdert werkzaamheid uit database
            if (listView_werk.SelectedValue == null)
            {
                return;
            }

            string query = "DELETE FROM Werk WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", listView_werk.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        private void btn_addWerk_Click(object sender, RoutedEventArgs e)
        {
            if (txtBox_Aantal.Text == "")
            {
                MessageBox.Show("Aantal niet ingevuld, er kan geen werkzaamheid zonder aantal worden toegevoegd!", "Aantal ontbreekt", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                string query = "INSERT INTO Werk (Werk, Aantal, PrijsPerEenheid, PrijsTotaal, PrijsTotaalIncl, Eenheid, BtwTarief, DossierId ,Omschrijving) VALUES (@Werk, @Aantal, @Prijs, @PrijsTotaal, @PrijsTotaalIncl, @Eenheid, @BtwTarief, @DossierId, @Omschrijving)";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@Werk", txtBox_Item.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                    command.Parameters.AddWithValue("@Aantal", txtBox_Aantal.Text);
                    command.Parameters.AddWithValue("@Prijs", txtBox_Prijs.Text);
                    command.Parameters.AddWithValue("@Eenheid", txtBox_Eenheid.Text);
                    command.Parameters.AddWithValue("@PrijsTotaal", txtBox_prijsTotaal.Text);
                    command.Parameters.AddWithValue("@PrijsTotaalIncl", txtBox_prijsTotaalIncl.Text);
                    command.Parameters.AddWithValue("@BtwTarief", txtBox_btwTarief.Text);
                    command.Parameters.AddWithValue("@DossierId", dossierId);
                    command.Parameters.AddWithValue("@Omschrijving", txtBox_Omschrijving.Text);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch
            {
                MessageBox.Show("Er is een probleem opgetreden, geen nieuwe werkzaamheid toegevoegd!", "Probleem", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Resultaten weergeven
            populateWerkzaamheden();
        }

        private void btn_wijzigWerk_Click(object sender, RoutedEventArgs e)
        {
            if (txtBox_Aantal.Text == "")
            {
                MessageBox.Show("Aantal niet ingevuld, er kan geen werkzaamheid zonder aantal worden toegevoegd!", "Aantal ontbreekt", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                string query = "UPDATE Werk SET Werk = @Werk, Aantal = @Aantal, PrijsPerEenheid = @Prijs, PrijsTotaal = @PrijsTotaal, PrijsTotaalIncl = @PrijsTotaalIncl, Eenheid = @Eenheid, BtwTarief = @BtwTarief, Omschrijving = @Omschrijving WHERE Id = @Id";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@Werk", txtBox_Item.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                    command.Parameters.AddWithValue("@Aantal", txtBox_Aantal.Text);
                    command.Parameters.AddWithValue("@Prijs", txtBox_Prijs.Text);
                    command.Parameters.AddWithValue("@Eenheid", txtBox_Eenheid.Text);
                    command.Parameters.AddWithValue("@PrijsTotaal", txtBox_prijsTotaal.Text);
                    command.Parameters.AddWithValue("@PrijsTotaalIncl", txtBox_prijsTotaalIncl.Text);
                    command.Parameters.AddWithValue("@BtwTarief", txtBox_btwTarief.Text);
                    command.Parameters.AddWithValue("@Omschrijving", txtBox_Omschrijving.Text);
                    command.Parameters.AddWithValue("@Id", werkId);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch
            {
                MessageBox.Show("Er is een probleem opgetreden, geen nieuwe werkzaamheid toegevoegd!", "Probleem", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //Resultaten weergeven
            populateWerkzaamheden();
        }

        //Comboboxes
        private void comboBox_filterWerk_Closed(object sender, EventArgs e)
        {
            populateComboBoxPrijslijst();
        }

        private void comboBox_prijslijst_Closed(object sender, EventArgs e)
        {
            if (comboBox_prijslijst.SelectedIndex == -1)
            {
                return;
            }
            txtBox_Item.Text = prijsTable.Rows[comboBox_prijslijst.SelectedIndex]["Item"].ToString();
            txtBox_Prijs.Text = prijsTable.Rows[comboBox_prijslijst.SelectedIndex]["Prijs"].ToString();
            txtBox_Eenheid.Text = prijsTable.Rows[comboBox_prijslijst.SelectedIndex]["Eenheid"].ToString();
            comboBox_Type.Text = prijsTable.Rows[comboBox_prijslijst.SelectedIndex]["Type"].ToString();
            txtBox_btwTarief.Text = prijsTable.Rows[comboBox_prijslijst.SelectedIndex]["BtwTarief"].ToString();

            Keyboard.Focus(txtBox_Aantal);
        }

        //Checkboxes     
        private void checkBox_aantal_Checked(object sender, RoutedEventArgs e)
        {
            calculatePrijs("Aantal_Changed"); //Op basis van het aantal wordt de berekening uitgevoerd
        }

        private void checkBox_totaalprijs_Checked(object sender, RoutedEventArgs e)
        {
            calculatePrijs("TotaalPrijs_Changed"); //Op basis van de totaalprijs wordt de berekening uitgevoerd
        }

        //Textboxes
        private void txtBox_Aantal_Changed(object sender, TextChangedEventArgs e)
        {
            calculatePrijs("Aantal_Changed");
        }

        private void txtBox_prijsTotaal_Changed(object sender, TextChangedEventArgs e)
        {
            calculatePrijs("TotaalPrijs_Changed");
        }

        private void txtBox_prijsTotaalIncl_Changed(object sender, TextChangedEventArgs e)
        {
            calculatePrijs("TotaalPrijsIncl_Changed");
        }


        //=================
        //Facturen:Algemeen
        //=================
        //Comboboxes
        private void comboBox_facturen_Closed(object sender, EventArgs e)
        {
            populateWerkzaamheden();
            populateFactuurDetails();
            calculateTotalenFactuur();
        }

        //=================
        //Facturen:Gegevens
        //=================
        //Buttons
        private void btn_newFactuur_Click(object sender, RoutedEventArgs e)
        {
            string query = "INSERT INTO Factuur (Datum, Status, DossierId) VALUES (@Datum, @Status, @DossierId)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Datum", DatePicker_factuurDatum.Text);
                command.Parameters.AddWithValue("@Status", txtBox_factuurStatus.Text);
                command.Parameters.AddWithValue("@DossierId", dossierId);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateComboBoxFacturen();
        }

        private void btn_editFactuur_Click(object sender, RoutedEventArgs e)
        {
            if (comboBox_facturen.SelectedValue == null)
            {
                return;
            }

            string query = "UPDATE Factuur SET Datum = @Datum, Status = @Status, DossierId = @DossierId WHERE Id = @Id";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Datum", DatePicker_factuurDatum.Text);
                command.Parameters.AddWithValue("@Status", txtBox_factuurStatus.Text);
                command.Parameters.AddWithValue("@DossierId", dossierId);
                command.Parameters.AddWithValue("@ID", comboBox_facturen.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateComboBoxFacturen();
        }

        private void btn_deleteFactuur_Click(object sender, RoutedEventArgs e)
        {
            //Verwijdert werkzaamheid uit database
            if (comboBox_facturen.SelectedValue == null)
            {
                return;
            }

            string query = "DELETE FROM Factuur WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", comboBox_facturen.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateComboBoxFacturen();
        }

        private void btn_setPartij1_Click(object sender, RoutedEventArgs e)
        {
            if (listView_AllKlanten.SelectedValue == null)
            {
                return;
            }

            string query = "IF NOT EXISTS (SELECT ID FROM DossierKlant WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 1') " +
                "UPDATE DossierKlant SET PartijNummer = 'Partij 1' WHERE Id = @Id";
            //"UPDATE Factuur SET Datum = @Datum, Status = @Status, DossierId = @DossierId WHERE Id = @Id";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Id", listView_AllKlanten.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        private void btn_setPartij2_Click(object sender, RoutedEventArgs e)
        {
            if (listView_AllKlanten.SelectedValue == null)
            {
                return;
            }

            string query = "IF NOT EXISTS (SELECT ID FROM DossierKlant WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 2') " +
                "UPDATE DossierKlant SET PartijNummer = 'Partij 2' WHERE Id = @Id";
            //"UPDATE Factuur SET Datum = @Datum, Status = @Status, DossierId = @DossierId WHERE Id = @Id";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Id", listView_AllKlanten.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        private void btn_removePartij1_Click(object sender, RoutedEventArgs e)
        {
            string query = "IF EXISTS (SELECT ID FROM DossierKlant WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 1') " +
                "UPDATE DossierKlant SET PartijNummer = '' WHERE PartijNummer = 'Partij 1'";
            //"UPDATE Factuur SET Datum = @Datum, Status = @Status, DossierId = @DossierId WHERE Id = @Id";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        private void btn_removePartij2_Click(object sender, RoutedEventArgs e)
        {
            string query = "IF EXISTS (SELECT ID FROM DossierKlant WHERE DossierId = '" + dossierId + "' AND PartijNummer = 'Partij 2') " +
                "UPDATE DossierKlant SET PartijNummer = '' WHERE PartijNummer = 'Partij 2'";
            //"UPDATE Factuur SET Datum = @Datum, Status = @Status, DossierId = @DossierId WHERE Id = @Id";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        //======================
        //Facturen:Werkzaamheden
        //======================
        //Buttons
        private void btn_addAllWerk_Click(object sender, RoutedEventArgs e)
        {
            if (comboBox_facturen.SelectedValue == null)
            {
                return;
            }

            string query = "INSERT INTO FactuurWerk (FactuurId, WerkId, DossierId) VALUES (@FactuurId, @WerkId, @DossierId)";

            foreach (DataRow row in werkTableFactuur.Rows)
            {
                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@FactuurId", comboBox_facturen.SelectedValue); //Parameters toewijzen, in dit geval tekst uit textbox
                    command.Parameters.AddWithValue("@WerkId", row["Id"]);
                    command.Parameters.AddWithValue("@DossierId", dossierId);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            populateWerkzaamheden();
        }

        private void btn_werkToevoegen_Click(object sender, RoutedEventArgs e)
        {
            if (comboBox_facturen.SelectedValue == null || listView_allWerk.SelectedValue == null)
            {
                return;
            }
            string query = "INSERT INTO FactuurWerk (FactuurId, WerkId, DossierId) VALUES (@FactuurId, @WerkId, @DossierId)";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@FactuurId", comboBox_facturen.SelectedValue); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@WerkId", listView_allWerk.SelectedValue);
                command.Parameters.AddWithValue("@DossierId", dossierId);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        private void btn_werkVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            //Verwijdert werkzaamheid uit database
            if (listView_addedWerk.SelectedValue == null)
            {
                return;
            }

            string query = "DELETE FROM FactuurWerk WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", listView_addedWerk.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateWerkzaamheden();
        }

        //Radiobuttons
        private void radioBtn_filterNotAdded_Checked(object sender, RoutedEventArgs e)
        {
            populateAllWerk();
        }

        private void radioBtn_filterAll_Checked(object sender, RoutedEventArgs e)
        {
            populateAllWerk();
        }

        //Listviews (selection changed)
        private void listView_werk_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string query = "SELECT * FROM Werk WHERE ID = '" + listView_werk.SelectedValue + "' ORDER BY Werk"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    DataTable currentWerkTable = new DataTable();
                    adapter.Fill(currentWerkTable);

                    txtBox_Item.Text = currentWerkTable.Rows[0]["Werk"].ToString();
                    txtBox_Aantal.Text = currentWerkTable.Rows[0]["Aantal"].ToString();
                    txtBox_prijsTotaal.Text = currentWerkTable.Rows[0]["PrijsTotaal"].ToString();
                    txtBox_Prijs.Text = currentWerkTable.Rows[0]["PrijsPerEenheid"].ToString();
                    txtBox_Eenheid.Text = currentWerkTable.Rows[0]["Eenheid"].ToString();
                    txtBox_prijsTotaalIncl.Text = currentWerkTable.Rows[0]["PrijsTotaalIncl"].ToString();
                    txtBox_Omschrijving.Text = currentWerkTable.Rows[0]["Omschrijving"].ToString();
                    txtBox_btwTarief.Text = currentWerkTable.Rows[0]["BtwTarief"].ToString();
                }
                werkId = int.Parse(listView_werk.SelectedValue.ToString());
            }
            catch { }
        }

        //=========================
        //Facturen:Verdeling kosten
        //=========================
        //Buttons
        private void btn_setForSelectedPartij1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string query = "UPDATE FactuurWerk SET PercPartij1 = @PercPartij1, PercPartij2 = @PercPartij2, PrijsPartij1 = @PrijsPartij1, PrijsPartij2 = @PrijsPartij2 WHERE WerkId = @Id";

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open(); //Openen van verbinding met database

                    command.Parameters.AddWithValue("@PercPartij1", txtBox_werkPercPartij1.Text);
                    command.Parameters.AddWithValue("@PercPartij2", txtBox_werkPercPartij2.Text);
                    command.Parameters.AddWithValue("@PrijsPartij1", txtBox_werkPrijsPartij1.Text);
                    command.Parameters.AddWithValue("@PrijsPartij2", txtBox_werkPrijsPartij2.Text);
                    command.Parameters.AddWithValue("@Id", listView_werkPartij1.SelectedValue);

                    command.ExecuteScalar(); //Toepassen van wijzigingen op db
                }
            }
            catch { }
            populateWerkPartijen();
        }

        private void btn_setForAllPartij1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (DataRow row in werkPartijenTable.Rows)
                {
                    string query = "UPDATE FactuurWerk SET PercPartij1 = @PercPartij1, PercPartij2 = @PercPartij2, PrijsPartij1 = @PrijsPartij1, PrijsPartij2 = @PrijsPartij2 WHERE WerkId = @WerkId";

                    using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        connection.Open(); //Openen van verbinding met database

                        decimal prijsPartij1 = decimal.Parse(row["PrijsTotaal"].ToString()) * decimal.Parse(txtBox_totPercPartij1.Text) / 100;
                        decimal prijsPartij2 = decimal.Parse(row["PrijsTotaal"].ToString()) * decimal.Parse(txtBox_totPercPartij2.Text) / 100;

                        command.Parameters.AddWithValue("@PercPartij1", txtBox_totPercPartij1.Text);
                        command.Parameters.AddWithValue("@PercPartij2", txtBox_totPercPartij2.Text);
                        command.Parameters.AddWithValue("@PrijsPartij1", prijsPartij1);
                        command.Parameters.AddWithValue("@PrijsPartij2", prijsPartij2);
                        command.Parameters.AddWithValue("@WerkId", row["Id"]);

                        command.ExecuteScalar(); //Toepassen van wijzigingen op db
                    }
                }
            }
            catch { }
            populateWerkPartijen();
        }

        private void btn_createFactuur_Click(object sender, RoutedEventArgs e)
        {
            createFactuurKlant();
        }

        //Listview(selection changed)
        private void listView_werkPartij1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                txtBox_werkPrijsTotaalPartij1.Text = werkPartijenTable.Rows[listView_werkPartij1.SelectedIndex]["PrijsTotaal"].ToString();
            }
            catch { }
        }

        //Textboxes(text changed)
        private void txtBox_werkPercPartij1_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_werkPercPartij1.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_werkPrijsTotaalPartij1.Text);
                decimal perc = decimal.Parse(txtBox_werkPercPartij1.Text);
                txtBox_werkPrijsPartij1.Text = (total * perc / 100).ToString("F");

                perc = 100 - perc;
                txtBox_werkPercPartij2.Text = perc.ToString();
                txtBox_werkPrijsPartij2.Text = (total * perc / 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_werkPrijsPartij1_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_werkPrijsPartij1.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_werkPrijsTotaalPartij1.Text);
                decimal part = decimal.Parse(txtBox_werkPrijsPartij1.Text);
                txtBox_werkPercPartij1.Text = (part / total * 100).ToString("F");

                part = total - part;
                txtBox_werkPrijsPartij2.Text = part.ToString();
                txtBox_werkPercPartij2.Text = (part / total * 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_totPercPartij1_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_totPercPartij1.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_totPrijsTotaalPartij1.Text);
                decimal perc = decimal.Parse(txtBox_totPercPartij1.Text);
                txtBox_totPrijsPartij1.Text = (total * perc / 100).ToString("F");

                perc = 100 - perc;
                txtBox_totPercPartij2.Text = perc.ToString();
                txtBox_totPrijsPartij2.Text = (total * perc / 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_totPrijsPartij1_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_totPrijsPartij1.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_totPrijsTotaalPartij1.Text);
                decimal part = decimal.Parse(txtBox_totPrijsPartij1.Text);
                txtBox_totPercPartij1.Text = (part / total * 100).ToString("F");

                part = total - part;
                txtBox_totPrijsPartij2.Text = part.ToString();
                txtBox_totPercPartij2.Text = (part / total * 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_werkPercPartij2_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_werkPercPartij2.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_werkPrijsTotaalPartij1.Text);
                decimal perc = decimal.Parse(txtBox_werkPercPartij2.Text);
                txtBox_werkPrijsPartij2.Text = (total * perc / 100).ToString("F");

                perc = 100 - perc;
                txtBox_werkPercPartij1.Text = perc.ToString();
                txtBox_werkPrijsPartij1.Text = (total * perc / 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_werkPrijsPartij2_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_werkPrijsPartij2.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_werkPrijsTotaalPartij1.Text);
                decimal part = decimal.Parse(txtBox_werkPrijsPartij2.Text);
                txtBox_werkPercPartij2.Text = (part / total * 100).ToString("F");

                part = total - part;
                txtBox_werkPrijsPartij1.Text = part.ToString();
                txtBox_werkPercPartij1.Text = (part / total * 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_totPercPartij2_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_totPercPartij2.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_totPrijsTotaalPartij1.Text);
                decimal perc = decimal.Parse(txtBox_totPercPartij2.Text);
                txtBox_totPrijsPartij2.Text = (total * perc / 100).ToString("F");

                perc = 100 - perc;
                txtBox_totPercPartij1.Text = perc.ToString();
                txtBox_totPrijsPartij1.Text = (total * perc / 100).ToString("F");
            }
            catch
            { }
        }

        private void txtBox_totPrijsPartij2_Changed(object sender, TextChangedEventArgs e)
        {
            if (txtBox_totPrijsPartij2.IsKeyboardFocused == false)
            {
                return;
            }
            try
            {
                decimal total = decimal.Parse(txtBox_totPrijsTotaalPartij1.Text);
                decimal part = decimal.Parse(txtBox_totPrijsPartij2.Text);
                txtBox_totPercPartij2.Text = (part / total * 100).ToString("F");

                part = total - part;
                txtBox_totPrijsPartij1.Text = part.ToString();
                txtBox_totPercPartij1.Text = (part / total * 100).ToString("F");
            }
            catch
            { }
        }

        //=================
        //Facturen:Facturen
        //=================
        //Buttons
        private void btn_CreateFactuurPartij1_Click(object sender, RoutedEventArgs e)
        {
            Form1_factuurcs from_factuurPartij1 = new Form1_factuurcs(int.Parse(comboBox_facturen.SelectedValue.ToString()), 1);
            from_factuurPartij1.Show();
        }

        private void btn_CreateFactuurPartij2_Click(object sender, RoutedEventArgs e)
        {
            Form1_factuurcs from_factuurPartij1 = new Form1_factuurcs(int.Parse(comboBox_facturen.SelectedValue.ToString()), 2);
            from_factuurPartij1.Show();
        }

        private void btn_toonBestanden_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            { }

            FileInfo fInfo = new FileInfo(openFileDialog.FileName);

            string strFileName = fInfo.Name;

            string strFilePath = fInfo.DirectoryName;

            WebBrowser_factuur.Navigate(strFilePath + "/" + strFileName);
        }
    }
}
