﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for Win_manageKlant.xaml
    /// </summary>
    public partial class Win_manageKlant : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        public Win_manageKlant()
        {
            InitializeComponent();

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;

            populateKlantenList();
            populateComboBox();
        }

        public void populateComboBox()
        {
            comboBox_filter.Items.Add("Alles");
            comboBox_filter.Items.Add("Naam");
            comboBox_filter.Items.Add("Plaats");

            comboBox_filter.SelectedIndex = 0;
        }

        public void populateKlantenList()
        {
            try
            {
                string query = "SELECT * FROM Klanten"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

                if (comboBox_filter.Text == "Naam")
                {
                    query += " WHERE Naam LIKE '" + txtBox_zoeken.Text + "%'";
                }
                else if (comboBox_filter.Text == "Plaats")
                {
                    query += " WHERE Plaats LIKE '" + txtBox_zoeken.Text + "%'";
                }

                using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
                using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
                {
                    DataTable klantTable = new DataTable();
                    adapter.Fill(klantTable);

                    listBox_klanten.SelectedValuePath = "Id";
                    listBox_klanten.ItemsSource = klantTable.DefaultView;
                }
            }
            catch { }
        }

        private void txtBox_zoeken_Changed(object sender, TextChangedEventArgs e)
        {
            populateKlantenList();
        }

        private void btn_openKlant_Click(object sender, RoutedEventArgs e)
        {
            if (listBox_klanten.SelectedValue == null)
            {
                return;
            }
            win_viewKlant winViewKlant1 = new win_viewKlant(int.Parse(listBox_klanten.SelectedValue.ToString()));
            winViewKlant1.Show();
        }

        private void btn_deleteKlant_Click(object sender, RoutedEventArgs e)
        {
            if (listBox_klanten.SelectedValue == null)
            {
                return;
            }

            MessageBoxResult result = MessageBox.Show("Weet u zeker dat u het geselecteerde klant wilt verwijderen?", "Verwijderen klant", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }

            string query = "DELETE FROM Klanten WHERE Id = @ID";

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@ID", listBox_klanten.SelectedValue);

                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }
            populateKlantenList();
        }

        private void btn_addKlant_Click(object sender, RoutedEventArgs e)
        {
            NieuweKlant winAddKlant = new NieuweKlant();    //Nieuwe window instantieren
            winAddKlant.Show(); //Openen van aangemaakte venster
        }

        private void comboBox_filter_Closed(object sender, EventArgs e)
        {
            populateKlantenList();

            Keyboard.Focus(txtBox_zoeken);
            txtBox_zoeken.SelectAll();
        }
    }
}
