﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// Toevoegen voor DB-operaties
using System.Configuration; //Ook toevoegen via menu: Project->Add reference, dan in de lijst System.configuration aanvinken
using System.Data.SqlClient;
using System.Data;

namespace PEB_database_try1
{
    /// <summary>
    /// Interaction logic for NieuweKlant.xaml
    /// </summary>
    public partial class NieuweKlant : Window
    {
        SqlConnection connection;   //Object gebruikt voor connectie met database
        string connectionString;    //Object waarin de connectionString wordt opgeslagen voor verbinding met database

        public NieuweKlant()
        {
            InitializeComponent();

            connectionString = PEB_database_try1.Properties.Settings.Default.Database_PEBConnectionString;
        }

        private void btn_AddKlant_Click(object sender, RoutedEventArgs e)
        {
            if (txtBox_naam.Text == "") //Controleer of er een naam is ingevuld, indien leeg -> melding weergeven
            {
                MessageBox.Show("Klantnaam is leeg, hierdoor is er geen nieuwe klant aangemaakt.", "Ontbrekende klantnaam", MessageBoxButton.OK,MessageBoxImage.Warning); //Messagebox pop-up met informatie
                return; //Stop toevoegen
            }

            string query = "INSERT INTO Klanten (Naam, Tussenvoegsel, Voornaam, AanvullendeNaam, ContactPersoon, "+
                "Telefoon, TelefoonContactPersoon, Email, Straat, Huisnummer, Postcode, Plaats, Postbus, PostcodePostbus,"+
                "PlaatsPostbus) VALUES (@Naam, @Tussenvoegsel, @Voornaam, @AanvullendeNaam, @ContactPersoon, @Telefoon, "+
                "@TelefoonContactPersoon, @Email, @Straat, @Huisnummer, @Postcode, @Plaats, @Postbus, @PostcodePostbus, "+
                "@PlaatsPostbus)"; //Query die je operatie definieert (@... is een parameter die later wordt ingevuld)

            using (connection = new SqlConnection(connectionString)) //using() maakt een omgeving waarbinnen je de parameters binnen haken gebruikt
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open(); //Openen van verbinding met database

                command.Parameters.AddWithValue("@Naam", txtBox_naam.Text); //Parameters toewijzen, in dit geval tekst uit textbox
                command.Parameters.AddWithValue("@Tussenvoegsel", txtBox_tussenvoegsel.Text);
                command.Parameters.AddWithValue("@Voornaam", txtBox_voornaam.Text);
                command.Parameters.AddWithValue("@AanvullendeNaam", txtBox_aanvullendeNaam.Text);
                command.Parameters.AddWithValue("@ContactPersoon", txtBox_contactPersoon.Text);
                command.Parameters.AddWithValue("@Telefoon", txtBox_telefoon.Text);
                command.Parameters.AddWithValue("@TelefoonContactPersoon", txtBox_telefoonContactPersoon.Text);
                command.Parameters.AddWithValue("@Email", txtBox_email.Text);
                command.Parameters.AddWithValue("@Straat", txtBox_straat.Text);
                command.Parameters.AddWithValue("@Huisnummer", txtBox_huisnummer.Text);
                command.Parameters.AddWithValue("@Postcode", txtBox_postcode.Text);
                command.Parameters.AddWithValue("@Plaats", txtBox_plaats.Text);
                command.Parameters.AddWithValue("@Postbus", txtBox_postbus.Text);
                command.Parameters.AddWithValue("@PostcodePostbus", txtBox_postcodePostbus.Text);
                command.Parameters.AddWithValue("@PlaatsPostbus", txtBox_plaatsPostbus.Text);
                command.ExecuteScalar(); //Toepassen van wijzigingen op db
            }

            //Message maken:
            string message = "";
            if (txtBox_voornaam.Text != "")
            {
                message += txtBox_voornaam.Text + " ";
            }

            if (txtBox_tussenvoegsel.Text !="")
            {
                message += txtBox_tussenvoegsel.Text + " ";
            }

            message += txtBox_naam.Text + " toegevoegd aan klantenbestand.";

            MessageBoxResult result = MessageBox.Show(message, "Klant toegevoegd", MessageBoxButton.OK, MessageBoxImage.Information);

            this.Close();
        }
    }
}
